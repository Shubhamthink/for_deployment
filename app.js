const express = require('express');
const app = express();
const port = 3000;

// Define a simple endpoint
app.get('/api', (req, res) => {
  res.json({ message: 'Hello, this is a simple API For Code Deployment!' });
});

// Start the server
app.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});
